import { useState, useEffect } from 'react'

const HatsList = () => {
  const [hats, setHats] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/hats/')
    if (response.ok){
      const data = await response.json();
      setHats(data.hats)
    }
  }

  useEffect(()=> {
    getData()
  }, [])

  const handleDelete = async (e) => {
    const url = `http://localhost:8090/api/hats/${e.target.id}`;

    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json"
      }
    }

    const response = await fetch(url, fetchConfig);
    const data = await response.json();

    if (response.ok){
      getData()
    }
  }

  return <>
        <div className='offset-2 col-8 bg-info'>
          <div className="shadow p-4 mt-4">
            <div className='flex justify-content-center'>
              <h1 className='text-center text-white'>A list of our Hats</h1>
            </div>
              <table className="table">
                  <thead>
                      <tr>
                          <th className='text-center'>Fabric</th>
                          <th className='text-center'>Style</th>
                          <th className='text-center'>Color</th>
                          <th className='text-center'>Picture</th>
                          <th className='text-center'>Location</th>
                          <th className='text-center'>Delete</th>
                      </tr>
                  </thead>
                  <tbody>
                      {
                      hats.map(hat => {
                          return (
                          <tr className='bg-light' key={hat.href}>
                              <td className='text-center'>{ hat.fabric }</td>
                              <td className='text-center'>{ hat.style_name }</td>
                              <td className='text-center'> { hat.color }</td>
                              <td className='text-center'><img src={ hat.picture_url } width={200} height={200} /></td>
                              <td className='text-center'>{ hat.location }</td>
                              <td className='text-center'><button onClick={handleDelete} id={hat.id} className="btn btn-danger">Delete</button></td>
                          </tr>
                          );
                      })
                      }
                  </tbody>
              </table>
          </div>
          </div>
    </>
}

export default HatsList
