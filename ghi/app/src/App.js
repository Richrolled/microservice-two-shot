import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatsList from './HatsList';
import MainPage from './MainPage';
import HatForm from './HatForm';
import Nav from './Nav';
import ShoeList from "./ShoeList"
import ShoeForm from './ShoeForm';
function App() {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="hats">
            <Route index element={<HatsList />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoeList />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>

          <Route path="/" element={<MainPage />} />
        </Routes>
    </BrowserRouter>
  );
}

export default App;
