import { useState, useEffect } from 'react'

const ShoesList = () => {
  const [shoes, setShoes] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/shoes/')
    if (response.ok){
      const data = await response.json();
      setShoes(data.shoes)
    }
  }

  useEffect(()=> {
    getData()
  }, [])

  const handleDelete = async (e) => {
    const url = `http://localhost:8080/api/shoes/${e.target.id}`;

    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json"
      }
    }

    const response = await fetch(url, fetchConfig);
    const data = await response.json();

    if (response.ok){
      getData()
    }
  }

  return <>
        <div className='offset-2 col-8 bg-info'>
          <div className="shadow p-4 mt-4">
            <div className='flex justify-content-center'>
              <h1 className='text-center text-white'>A list of our Shoes</h1>
            </div>
              <table className="table">
                  <thead>
                      <tr>
                          <th className='text-center'>Manufacturer</th>
                          <th className='text-center'>Model</th>
                          <th className='text-center'>Color</th>
                          <th className='text-center'>Picture</th>
                          <th className='text-center'>Bin</th>
                          <th className='text-center'>Delete</th>
                      </tr>
                  </thead>
                  <tbody>
                      {
                      shoes.map(shoe => {
                          return (
                          <tr className='bg-light' key={shoe.href}>
                              <td className='text-center'>{ shoe.manufacturer }</td>
                              <td className='text-center'>{ shoe.model_name }</td>
                              <td className='text-center'> { shoe.color }</td>
                              <td className='text-center'><img src={ shoe.picture_url } width={200} height={200} /></td>
                              <td className='text-center'>{ shoe.bin }</td>
                              <td className='text-center'><button onClick={handleDelete} id={shoe.id} className="btn btn-danger">Delete</button></td>
                          </tr>
                          );
                      })
                      }
                  </tbody>
              </table>
          </div>
          </div>
    </>
}

export default ShoesList
