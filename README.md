# Wardrobify

Team:

* Richard - Hats
* Gabriel - Shoes

## Design
Project styled with Bootstrap.

## Shoes microservice

You can add different types of shoes to the wardrobe app.

## Hats microservice

The models made in the models.py folder under hats_rest folder are the Hat and LocationVO models.
The poller, gets data from http://wardrobe-api:8000/api/locations/ every 60 seconds and checks if
there have been any new locations added. It compares the data from the poll to the data held for
the LocationVO model. If there are any updates to existing LocationVOs or new LocationVOs, the poller
will update or create them. The LocationVO model is filled with data from the Location model in wardrobe.
A hat is created using when Hat model is filled with data from the HatForm, which sends a post request
when submitted.
